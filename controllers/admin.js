const bcrypt = require("bcryptjs");

const User = require("../models/user");
const Location = require("../models/location");
const Restaurant = require("../models/restaurant");
const RestaurantBranch = require("../models/restaurantBranch");

exports.test = (req, res, next) => res.json({ msg: "Admin Works" });

exports.createUser = (req, res, next) => {
  User.findByEmail(req.body.email).then(([user]) => {
    if (user[0]) {
      return res.status(400).json("Email already exists");
    } else if (req.user[0].roleId !== 1) {
      return res.status(403).send(`User is not authorized`);
    } else {
      bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(req.body.password, salt, (err, hash) => {
          if (err) {
            throw err;
          }
          let roleId = req.body.role === "admin" ? 1 : 2;
          const newUser = new User(
            null,
            req.body.name,
            req.body.email,
            hash,
            roleId
          );
          return newUser
            .save()
            .then(() => res.status(200).json(`user Created`))
            .catch(err => console.log(err));
        });
      });
    }
  });
};

exports.createLocation = (req, res, next) => {
  Location.findByName(req.body.name).then(([location]) => {
    if (location[0]) {
      return res.status(400).json("Location already exists");
    } else if (req.user[0].roleId !== 1) {
      return res.status(403).send("User is not authorized");
    } else {
      let newLocation = new Location(null, req.body.name);
      return newLocation
        .save()
        .then(location => res.status(200).json(`location Created`))
        .catch(err => console.log(err));
    }
  });
};

exports.createRestaurant = (req, res, next) => {
  User.findByEmail(req.body.ownerEmail).then(([owner]) => {
    if (!owner[0]) {
      return res.status(400).json("Owner not exists");
    } else if (req.user[0].roleId !== 1) {
      return res
        .status(403)
        .send("User is not authorized ot create restaurant");
    } else {
      let newRest = new Restaurant(null, req.body.restaurantName, owner[0].id);
      return newRest
        .save()
        .then(() => res.status(200).json(`restaurant created`))
        .catch(err => console.log(err));
    }
  });
};

exports.createRestaurantBranch = (req, res, next) => {
  Restaurant.findByName(req.body.restaurantName).then(([restaurant]) => {
    if (!restaurant[0]) {
      return res.status(400).json("Restaurant not exists");
    } else if (req.user[0].roleId !== 1) {
      return res
        .status(403)
        .send("User is not authorized ot create restaurant branch");
    } else {
      Location.findByName(req.body.locationName).then(([location]) => {
        if (!location[0]) {
          return res.status(400).json("location not exists");
        } else {
          let newBranch = new RestaurantBranch(
            null,
            req.body.branchName,
            restaurant[0].id,
            location[0].id
          );
          return newBranch
            .save()
            .then(result => res.status(200).json(`branch created`))
            .catch(err => console.log(err));
        }
      });
    }
  });
};
