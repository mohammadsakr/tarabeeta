const express = require("express");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const keys = require("../config/keys");

// Load  model
const User = require("../models/user");
const Restaurant = require("../models/restaurant");
const Menu = require("../models/menu");
const MenuItem = require("../models/menuItem");
const RestaurantBranch = require("../models/restaurantBranch");

exports.test = (req, res, next) => res.json({ msg: "Users Works" });

exports.login = (req, res, next) => {
  const email = req.body.email;
  const password = req.body.password;
  console.log(`password and email are ${email} ${password}`);
  // Find user by email
  User.findByEmail(email).then(([user]) => {
    // Check for user
    if (!user[0]) {
      return res.status(404).json("User not found");
    }
    // Check Password
    bcrypt.compare(password, user[0].password).then(isMatch => {
      if (isMatch) {
        // User Matched
        const payload = {
          id: user[0].id,
          name: user[0].name,
          roleId: user[0].roleId
        }; // Create JWT Payload

        // Sign Token
        jwt.sign(
          payload,
          keys.secretOrKey,
          { expiresIn: 3600 },
          (err, token) => {
            res.json({
              success: true,
              token: "Bearer " + token
            });
          }
        );
      } else {
        return res.status(400).json(`Error`);
      }
    });
  });
};

exports.register = (req, res, next) => {
  User.findByEmail(req.body.email).then(([user]) => {
    if (user[0]) {
      return res.status(400).json("Email already exists");
    } else {
      bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(req.body.password, salt, (err, hash) => {
          if (err) {
            throw err;
          }
          let roleId = 2;
          const newUser = new User(
            null,
            req.body.name,
            req.body.email,
            hash,
            roleId
          );
          return newUser
            .save()
            .then(() => res.status(200).json(`user Created`))
            .catch(err => console.log(err));
        });
      });
    }
  });
};

exports.current = (req, res, next) => {
  res.json({
    id: req.user[0].id,
    name: req.user[0].name,
    email: req.user[0].email,
    roleId: req.user[0].roleId
  });
};

exports.createMenu = (req, res, next) => {
  Restaurant.findByName(req.body.restaurantName).then(([restaurant]) => {
    if (!restaurant[0]) {
      return res.status(400).json("restaurant not exists");
    } else if (req.user[0].id !== restaurant[0].userId) {
      return res
        .status(403)
        .send("User is not authorized ot create menu for this restaurant");
    } else {
      let newMenu = new Menu(null, req.body.menuName, req.user[0].id);
      return newMenu
        .save()
        .then(() => res.status(200).json(`menu created`))
        .catch(err => console.log(err));
    }
  });
};

exports.createMenuItem = (req, res, next) => {
  Restaurant.findByName(req.body.restaurantName).then(([restaurant]) => {
    if (!restaurant[0]) {
      return res.status(400).json("restaurant not exists");
    } else if (req.user[0].id !== restaurant[0].userId) {
      return res
        .status(403)
        .send("User is not authorized ot create menu for this restaurant");
    } else {
      Menu.findByName(req.body.menuName).then(([menu]) => {
        if (!menu[0]) {
          return res.status(400).json("Menu not exists");
        } else if (menu[0].restaurantId !== restaurant[0].id) {
          console.log(`menu ${menu[0].id} & restaurant ${restaurant[0].id} `);
          return res.status(403).send("Menu is not related to that resturant");
        } else {
          let newMenuItem = new MenuItem(
            null,
            req.body.title,
            req.body.image,
            req.body.description,
            req.body.price,
            menu[0].id
          );
          return newMenuItem
            .save()
            .then(() => res.status(200).json(`Menu item created`))
            .catch(err => console.log(err));
        }
      });
    }
  });
};

exports.AllMenus = (req, res, next) => {
  Menu.findAllMenusByOwnerId(req.user[0].id)
    .then(([menus]) => {
      console.log(req.user[0].id);
      console.log(menus);
      res.status(200).json(menus);
    })
    .catch(err => console.log(err));
};

exports.AllMenuItems = (req, res, next) => {
  MenuItem.findAllMenuItemsByOwnerId(req.user[0].id, req.body.menuName)
    .then(([menuItems]) => {
      res.status(200).json(menuItems);
    })
    .catch(err => console.log(err));
};

exports.AllBranches = (req, res, next) => {
  RestaurantBranch.findAllBranchesByOwnerId(req.user[0].id)
    .then(([branches]) => {
      res.status(200).json(branches);
    })
    .catch(err => console.log(err));
};
