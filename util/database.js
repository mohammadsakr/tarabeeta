const mysql = require("mysql2");

const pool = mysql.createPool({
  database: "tarbeeta_db",
  user: "root",
  password: "root",
  host: "localhost"
});

module.exports = pool.promise();
