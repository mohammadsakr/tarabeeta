const db = require("../util/database");
module.exports = class Menu {
  constructor(id, name, restaurantId) {
    this.id = id;
    this.name = name;
    this.restaurantId = restaurantId;
  }

  save() {
    return db.execute("INSERT INTO Menus (name, restaurantId) VALUES (?,?)", [
      this.name,
      this.restaurantId
    ]);
  }

  static findByName(name) {
    return db.execute("SELECT * FROM Menus WHERE Menus.name = ?", [name]);
  }

  static findAllMenusByOwnerId(userId) {
    return db.execute(
      `SELECT m.id MenuID, m.name MenuName, r.name RestaurantName
      FROM tarbeeta_db.users u
      inner join tarbeeta_db.restaurants r on u.id = r.userid 
      inner join tarbeeta_db.menus m on m.restaurantId = r.id
    where u.id = ?`,
      [userId]
    );
  }
};
