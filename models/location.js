const db = require("../util/database");
module.exports = class Location {
  constructor(id, name) {
    this.id = id;
    this.name = name;
  }

  save() {
    return db.execute("INSERT INTO locations (name) VALUES (?)", [this.name]);
  }

  static findByName(name) {
    return db.execute("SELECT * FROM locations WHERE locations.name = ?", [
      name
    ]);
  }
};
