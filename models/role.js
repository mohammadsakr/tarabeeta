const db = require("../util/database");
module.exports = class Role {
  constructor(id, name) {
    this.id = id;
    this.name = name;
  }

  save() {
    return db.execute("INSERT INTO roles (name) VALUES (?)", [this.name]);
  }

  static findByName(name) {
    return db.execute("SELECT * FROM roles WHERE roles.name = ?", [name]);
  }
};
