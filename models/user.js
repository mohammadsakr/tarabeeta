const db = require("../util/database");

module.exports = class User {
  constructor(id, name, email, password, roleId) {
    this.id = id;
    this.name = name;
    this.email = email;
    this.password = password;
    this.roleId = roleId;
  }

  save() {
    return db.execute(
      "INSERT INTO users (name, email, password, roleId) VALUES (?, ?, ?, ?)",
      [this.name, this.email, this.password, this.roleId]
    );
  }

  static findById(id) {
    return db.execute("SELECT * FROM users WHERE users.id = ?", [id]);
  }

  static findByEmail(email) {
    return db.execute("SELECT * FROM users WHERE users.email = ?", [email]);
  }
};
