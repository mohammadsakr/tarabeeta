const Sequelize = require("sequelize");

const db = require("../util/database");
module.exports = class Restaurant {
  constructor(id, title, image, description, price, menuId) {
    this.id = id;
    this.title = title;
    this.image = image;
    this.description = description;
    this.price = price;
    this.menuId = menuId;
  }

  save() {
    return db.execute(
      "INSERT INTO menuitems (title,image,description, price, menuId) VALUES (?,?,?,?,?)",
      [this.title, this.image, this.description, this.price, this.menuId]
    );
  }

  static findByName(name) {
    return db.execute("SELECT * FROM menuitems WHERE menuitems.name = ?", [
      name
    ]);
  }

  static findAllMenuItemsByOwnerId(userId, menuName) {
    return db.execute(
      `SELECT mi.id MenuItemID, mi.title Title, mi.image Image  , mi.description Description, mi.price, m.name MenuName
      FROM tarbeeta_db.menuItems mi
      inner join tarbeeta_db.menus m on m.id = mi.menuId
      inner join tarbeeta_db.restaurants r on m.restaurantId = r.id
      inner join tarbeeta_db.users u on u.id = r.userid
      where u.id = ? and m.name = ?`,
      [userId, menuName]
    );
  }
};
