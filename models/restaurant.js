const db = require("../util/database");
module.exports = class Restaurant {
  constructor(id, name, userId) {
    this.id = id;
    this.name = name;
    this.userId = userId;
  }

  save() {
    return db.execute("INSERT INTO Restaurants (name, userId) VALUES (?,?)", [
      this.name,
      this.userId
    ]);
  }

  static findByName(name) {
    return db.execute("SELECT * FROM Restaurants WHERE Restaurants.name = ?", [
      name
    ]);
  }
};
