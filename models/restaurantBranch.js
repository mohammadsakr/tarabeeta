const db = require("../util/database");
module.exports = class RestaurantBranch {
  constructor(id, name, restaurantId, locationId) {
    this.id = id;
    this.name = name;
    this.restaurantId = restaurantId;
    this.locationId = locationId;
  }

  save() {
    return db.execute(
      "INSERT INTO restaurantbranches (name,restaurantId, locationId) VALUES (?,?,?)",
      [this.name, this.restaurantId, this.locationId]
    );
  }

  static findByName(name) {
    return db.execute(
      "SELECT * FROM restaurantbranches WHERE locations.name = ?",
      [name]
    );
  }

  static findAllBranchesByOwnerId(userId) {
    return db.execute(
      `SELECT b.id BranchID, b.name BranchName, r.name RestaurantName  , l.name LocationName
      FROM tarbeeta_db.users u
      left join tarbeeta_db.restaurants r  on u.id = r.userid
      left join  tarbeeta_db.restaurantbranches b on b.restaurantId = r.id
      left join tarbeeta_db.locations l on l.id = b.locationId
      where b.id is not null and u.id = ?`,
      [userId]
    );
  }
};
