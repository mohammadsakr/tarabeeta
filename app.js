const express = require("express");
const bodyParser = require("body-parser");
const passport = require("passport");

const db = require("./util/database");

const adminRoutes = require("./routes/api/admin");
const userRoutes = require("./routes/api/user");

const app = express();
const port = process.env.PORT || 3000;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Passport middleware
app.use(passport.initialize());

// Passport Config
require("./config/passport")(passport);

app.use("/api/admin", adminRoutes);
app.use("/api/user", userRoutes);

app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
